var WIDTH = 10
var HEIGHT = 10;

var BLANK = 0;
var WHITE = 1;
var WHITE_Q = 2;
var BLACK = 3;
var BLACK_Q = 4;

var board = [
	BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK,
	BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK,
	BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK,
	BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK, BLACK, BLANK,
	BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,
	BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,
	BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE,
	WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK,
	BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE,
	WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK, WHITE, BLANK,
];

var squares = [];
var lastSelection = null;
var neightbour = [];

$(document).ready(function(){
	var y = 0;
	while (y < HEIGHT){
		var x = 0;
		while (x < WIDTH){
			$("#board").append(
				'<div class="square white" x="' + x +
				'" y="' + y +
				'" onclick="squareClicked(this)"></div>'
			);
			x++;
			$("#board").append(
				'<div class="square black" x="' + x +
				'" y="' + y +
				'" onclick="squareClicked(this)"></div>'
			);
			x++;
		}
		y++;
		x = 0
		while (x < WIDTH){
			$("#board").append(
				'<div class="square black" x="' + x +
				'" y="' + y +
				'" onclick="squareClicked(this)"></div>'
			);
			x++;
			$("#board").append(
				'<div class="square white" x="' + x +
				'" y="' + y +
				'" onclick="squareClicked(this)"></div>'
			);
			x++;
		}
		y++;
	}

	squares = $("#board").children();
	for (var y = 0; y < HEIGHT; y++){
		for (var x = 0; x < WIDTH; x++){
			if (board[x + y * WIDTH] == 1){
				$(squares[x + y * WIDTH]).append(
					'<img class="pawn" src="white.png" x="' + x + 
					'" y="' + y + '" />'
				);
			}
			else if (board[x + y * WIDTH] == 3){
				$(squares[x + y * WIDTH]).append(
					'<img class="pawn" src="black.png" x="' + x + 
					'" y="' + y + '" />'
				);
			}
		}
	}
});

function squareClicked(target){
	if (lastSelection != null){
		lastSelection.removeClass("selected");
	}
	// alert("x="+$(target).attr("x")+", y="+$(target).attr("y"));
	lastSelection = $(target);

	lastSelection.addClass("selected");
}
